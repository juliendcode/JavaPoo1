public class Classroom {

    public static void main(String[] args) {

        Wilder jim = new Wilder("Jim", true);
        System.out.println(jim.whoAmI());

        Wilder joe = new Wilder("Joe", false);
        System.out.println(joe.whoAmI());

    }
}