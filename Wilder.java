public class Wilder {
    private String firstname;
    private boolean aware;

    public Wilder(String firstname, boolean aware) {
        this.firstname = firstname;
        this.aware = aware;
    }

    public String whoAmI() {
        if (this.aware == true) {
            return "je m'appelle " + this.getFirstName() + " et je suis aware";
        } else {
            return "je m'appelle " + this.getFirstName() + " et je ne suis pas du tout aware";
        }
    }

    public String getFirstName() {
        return this.firstname;
    }

    public void setFirstName(String firstname) {
        this.firstname = firstname;
    }

    public boolean isAware() {
        return this.aware;

    }

    public void setAware() {
        this.aware = aware;
    }
}
